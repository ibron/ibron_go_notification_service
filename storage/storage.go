package storage

import (
	"context"
	"ibron/ibron_go_notification_service.git/genproto/notification_service"
)

type StorageI interface {
	CloseDB()
	Notification() NotificationRepoI
	SendNotification() SendNotificationRepoI
}

type NotificationRepoI interface {
	Create(ctx context.Context, req *notification_service.CreateNotification) (resp *notification_service.NotificationPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *notification_service.NotificationPrimaryKey) (resp *notification_service.Notification, err error)
	GetAll(ctx context.Context, req *notification_service.GetListNotificationRequest) (resp *notification_service.GetListNotificationResponse, err error)
	Update(ctx context.Context, req *notification_service.UpdateNotification) (rowsAffected int64, err error)
	HardDelete(ctx context.Context, req *notification_service.NotificationPrimaryKey) error
	SoftDelete(ctx context.Context, req *notification_service.NotificationPrimaryKey) error
}

type SendNotificationRepoI interface {
	Create(ctx context.Context, req *notification_service.CreateSendNotification) (resp *notification_service.SendNotificationPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *notification_service.SendNotificationPrimaryKey) (resp *notification_service.SendNotification, err error)
	GetAll(ctx context.Context, req *notification_service.GetListSendNotificationRequest) (resp *notification_service.GetListSendNotificationResponse, err error)	
	SoftDelete(ctx context.Context, req *notification_service.SendNotificationPrimaryKey) error
}
