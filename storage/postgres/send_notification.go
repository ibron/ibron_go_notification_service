package postgres

import (
	"context"
	"ibron/ibron_go_notification_service.git/genproto/notification_service"
	"ibron/ibron_go_notification_service.git/pkg/helper"
	"ibron/ibron_go_notification_service.git/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type SendNotificationRepo struct {
	db *pgxpool.Pool
}

func NewSendNotificationRepo(db *pgxpool.Pool) storage.SendNotificationRepoI {
	return &SendNotificationRepo{
		db: db,
	}
}


func (c *SendNotificationRepo) Create(ctx context.Context, req *notification_service.CreateSendNotification) (resp *notification_service.SendNotificationPrimaryKey, err error) {

	var send_id = uuid.New().String()

	query := `INSERT INTO "send_notifications" (
					send_id,
					notification_id,
					users_id,
					send_at
				) VALUES ($1, $2, $3, now())
	`

	_, err = c.db.Exec(ctx, query, 
		send_id,
		req.NotificationId,
		req.UsersId,
	)

	if err != nil{
		return nil, err
	}

	return &notification_service.SendNotificationPrimaryKey{SendId: send_id}, nil
}

func (c *SendNotificationRepo) GetByPKey(ctx context.Context, req *notification_service.SendNotificationPrimaryKey) (*notification_service.SendNotification, error) {

	query := `
		SELECT
			send_id,
			notification_id,
			users_id,
			TO_CHAR(send_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "send_notifications"
		WHERE send_id = $1 AND deleted_at is null
	`
	var resp notification_service.SendNotification

	err := c.db.QueryRow(ctx, query, req.GetSendId()).Scan(
		&resp.SendId,
		&resp.NotificationId,
		&resp.UsersId,
		&resp.SendAt,
		&resp.DeletedAt,
	)

	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *SendNotificationRepo) GetAll(ctx context.Context, req *notification_service.GetListSendNotificationRequest) (resp *notification_service.GetListSendNotificationResponse, err error) {

	resp = &notification_service.GetListSendNotificationResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			send_id,
			notification_id,
			users_id,
			TO_CHAR(send_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "send_notifications"
		WHERE deleted_at is null
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}


	query += offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var s_notification notification_service.SendNotification

		err := rows.Scan(
			&resp.Count,
			&s_notification.SendId,
			&s_notification.NotificationId,
			&s_notification.UsersId,
			&s_notification.SendAt,
			&s_notification.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.SendNotifications = append(resp.SendNotifications, &s_notification)
	}

	return
}

func (c *SendNotificationRepo) SoftDelete(ctx context.Context, req *notification_service.SendNotificationPrimaryKey) error {

	query := `UPDATE send_notifications SET deleted_at = now() WHERE send_id = $1`

	_, err := c.db.Exec(ctx, query, req.GetSendId())

	if err != nil {
		return err
	}

	return nil
}