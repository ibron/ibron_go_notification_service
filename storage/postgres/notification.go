package postgres

import (
	"context"
	"ibron/ibron_go_notification_service.git/genproto/notification_service"
	"ibron/ibron_go_notification_service.git/storage"
	"ibron/ibron_go_notification_service.git/pkg/helper"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/google/uuid"
)

type NotificationRepo struct {
	db *pgxpool.Pool
}

func NewNotificationRepo(db *pgxpool.Pool) storage.NotificationRepoI {
	return &NotificationRepo{
		db: db,
	}
}

func (c *NotificationRepo) Create(ctx context.Context, req *notification_service.CreateNotification) (resp *notification_service.NotificationPrimaryKey, err error) {

	var notification_id = uuid.New()

	query := `INSERT INTO "notifications" (
				notification_id,
				title,
				description,
				updated_at
			) VALUES ($1, $2, $3, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		notification_id.String(),
		req.GetTitle(),
		req.GetDescription(),
	)

	if err != nil {
		return nil, err
	}

	return &notification_service.NotificationPrimaryKey{NotificationId: notification_id.String()}, nil
}

func (c *NotificationRepo) GetByPKey(ctx context.Context, req *notification_service.NotificationPrimaryKey) (*notification_service.Notification, error) {

	query := `
		SELECT
			notification_id,
			title,
			description,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "notifications"
		WHERE notification_id = $1
	`
	var resp notification_service.Notification

	err := c.db.QueryRow(ctx, query, req.NotificationId).Scan(
		&resp.NotificationId,
		&resp.Title,
		&resp.Description,
		&resp.CreatedAt,
		&resp.UpdatedAt,
		&resp.DeletedAt,
	)

	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *NotificationRepo) GetAll(ctx context.Context, req *notification_service.GetListNotificationRequest) (resp *notification_service.GetListNotificationResponse, err error) {

	resp = &notification_service.GetListNotificationResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			notification_id,
			title,
			description,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "notifications"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetSearch()) > 0 {
		filter = " WHERE title ILIKE" + "'%" + req.Search + "%'"
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var notification notification_service.Notification

		err := rows.Scan(
			&resp.Count,
			&notification.NotificationId,
			&notification.Title,
			&notification.Description,
			&notification.CreatedAt,
			&notification.UpdatedAt,
			&notification.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Notifications = append(resp.Notifications, &notification)
	}

	return
}

func (c *NotificationRepo) Update(ctx context.Context, req *notification_service.UpdateNotification) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "notifications"
			SET
				title = $1,
				description = $2
			WHERE
				notification_id = $3`

	result, err := c.db.Exec(ctx, query,
		req.GetTitle(),
		req.GetDescription(),
		req.GetNotificationId(),
	)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *NotificationRepo) HardDelete(ctx context.Context, req *notification_service.NotificationPrimaryKey) error {

	query := `DELETE FROM "notifications" WHERE notification_id = $1`

	_, err := c.db.Exec(ctx, query, req.GetNotificationId())

	if err != nil {
		return err
	}

	return nil
}

func (c *NotificationRepo) SoftDelete(ctx context.Context, req *notification_service.NotificationPrimaryKey) error {

	query := `UPDATE notifications SET deleted_at = now() WHERE notification_id = $1`

	_, err := c.db.Exec(ctx, query, req.GetNotificationId())

	if err != nil {
		return err
	}

	return nil
}

