package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"


	"ibron/ibron_go_notification_service.git/config"
	"ibron/ibron_go_notification_service.git/genproto/notification_service"
	"ibron/ibron_go_notification_service.git/grpc/client"
	"ibron/ibron_go_notification_service.git/grpc/service"
	"ibron/ibron_go_notification_service.git/pkg/logger"
	"ibron/ibron_go_notification_service.git/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	notification_service.RegisterNotificationServiceServer(grpcServer, service.NewNotificationService(cfg, log, strg, srvc))
	notification_service.RegisterSendNotificationServiceServer(grpcServer, service.NewSendNotificationService(cfg, log, strg, srvc))


	reflection.Register(grpcServer)
	return
}
