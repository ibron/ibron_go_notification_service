package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"ibron/ibron_go_notification_service.git/config"
	"ibron/ibron_go_notification_service.git/genproto/notification_service"
	"ibron/ibron_go_notification_service.git/grpc/client"
	"ibron/ibron_go_notification_service.git/pkg/logger"
	"ibron/ibron_go_notification_service.git/storage"
)

type SendNotificationService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*notification_service.UnimplementedSendNotificationServiceServer
}

func NewSendNotificationService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *SendNotificationService {
	return &SendNotificationService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *SendNotificationService) Create(ctx context.Context, req *notification_service.CreateSendNotification) (resp *notification_service.SendNotification, err error) {

	i.log.Info("---CreateSendNotification------>", logger.Any("req", req))

	pKey, err := i.strg.SendNotification().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateSendNotification->SendNotification->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.SendNotification().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyNotification->Notification->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SendNotificationService) GetById(ctx context.Context, req *notification_service.SendNotificationPrimaryKey) (resp *notification_service.SendNotification, err error) {

	i.log.Info("---GetSendNotificationByID------>", logger.Any("req", req))

	resp, err = i.strg.SendNotification().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSendNotificationByID->SendNotification->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SendNotificationService) GetList(ctx context.Context, req *notification_service.GetListSendNotificationRequest) (resp *notification_service.GetListSendNotificationResponse, err error) {

	i.log.Info("---GetSendNotifications------>", logger.Any("req", req))

	resp, err = i.strg.SendNotification().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSendNotification->SendNotification->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SendNotificationService) SoftDelete(ctx context.Context, req *notification_service.SendNotificationPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteSendNotification------>", logger.Any("req", req))

	err = i.strg.SendNotification().SoftDelete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteSendNotification->SendNotification->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
