package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"ibron/ibron_go_notification_service.git/config"
	"ibron/ibron_go_notification_service.git/genproto/notification_service"
	"ibron/ibron_go_notification_service.git/grpc/client"
	"ibron/ibron_go_notification_service.git/pkg/logger"
	"ibron/ibron_go_notification_service.git/storage"
)

type NotificationService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*notification_service.UnimplementedNotificationServiceServer
}

func NewNotificationService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *NotificationService {
	return &NotificationService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *NotificationService) Create(ctx context.Context, req *notification_service.CreateNotification) (resp *notification_service.Notification, err error) {

	i.log.Info("---CreateNotification------>", logger.Any("req", req))

	pKey, err := i.strg.Notification().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateNotification->Notification->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Notification().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyNotification->Notification->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *NotificationService) GetById(ctx context.Context, req *notification_service.NotificationPrimaryKey) (resp *notification_service.Notification, err error) {

	i.log.Info("---GetNotificationByID------>", logger.Any("req", req))

	resp, err = i.strg.Notification().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetNotificationByID->Notification->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *NotificationService) GetList(ctx context.Context, req *notification_service.GetListNotificationRequest) (resp *notification_service.GetListNotificationResponse, err error) {

	i.log.Info("---GetNotifications------>", logger.Any("req", req))

	resp, err = i.strg.Notification().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetNotification->Notification->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *NotificationService) Update(ctx context.Context, req *notification_service.UpdateNotification) (resp *notification_service.Notification, err error) {

	i.log.Info("---UpdateNotification------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Notification().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateNotification--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Notification().GetByPKey(ctx, &notification_service.NotificationPrimaryKey{NotificationId: req.NotificationId})
	if err != nil {
		i.log.Error("!!!GetNotification->Notification->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *NotificationService) HardDelete(ctx context.Context, req *notification_service.NotificationPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteNotification------>", logger.Any("req", req))

	err = i.strg.Notification().HardDelete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteNotification->Notification->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}

func (i *NotificationService) SoftDelete(ctx context.Context, req *notification_service.NotificationPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteNotification------>", logger.Any("req", req))

	err = i.strg.Notification().SoftDelete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteNotification->Notification->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
