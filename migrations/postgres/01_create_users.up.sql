
CREATE TABLE IF NOT EXISTS "notifications" (
    "notification_id" uuid PRIMARY KEY NOT NULL,
    "title" VARCHAR NOT NULL,
    "description" VARCHAR NOT NULL,
    "created_at" TIMESTAMP DEFAULT (now()),
    "updated_at" TIMESTAMP DEFAULT (now()),
    "deleted_at" TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "send_notifications" (
    "send_id" uuid PRIMARY KEY NOT NULL,
    "notification_id" uuid REFERENCES notifications(notification_id),
    "users_id" VARCHAR[] NOT NULL,
    "send_at" TIMESTAMP DEFAULT (now()),
    "deleted_at" TIMESTAMP
);